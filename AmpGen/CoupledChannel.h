#include "AmpGen/Expression.h"
#include "AmpGen/Particle.h"

namespace AmpGen { 
  Expression phaseSpace(const Expression& s, const Particle& p, const size_t& l);
}
